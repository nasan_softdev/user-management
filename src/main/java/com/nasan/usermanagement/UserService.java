/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nasan.usermanagement;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author nasan
 */
public class UserService {

    private static ArrayList<User> userList = new ArrayList<>();

    // Mockup
    static {
        load();
        if (userList.isEmpty()) {
            userList.add(new User("admin", "password"));
        } else {
            if (!adminCheck()) {
                userList.add(new User("admin", "password"));
            }
        }
    }

    // Create
    public static boolean addUser(User user) {
        userList.add(user);
        return true;
    }

    public static boolean addUser(String userName, String password) {
        userList.add(new User(userName, password));
        return true;
    }

    // Update
    public static boolean updateUser(int index, User user) {
        userList.set(index, user);
        return true;
    }

    // Read1user
    public static User getUser(int index) {
        if (index > userList.size() - 1) {
            return null;
        }
        return userList.get(index);
    }

    // Read all user
    public static ArrayList<User> getUsers() {
        return userList;
    }

    // Search username
    public static ArrayList<User> searchUserName(String searchText) {
        ArrayList<User> list = new ArrayList<>();
        for (User user : userList) {
            if (user.getUserName().startsWith(searchText)) {
                list.add(user);
            }
        }
        return list;
    }

    // Delete user by index
    public static boolean delUser(int index) {
        userList.remove(index);
        save();
        return true;
    }

    // Delete user by object
    public static boolean delUser(User user) {
        userList.remove(user);
        save();
        return true;
    }

    // Login
    public static User login(String userName, String password) {
        for (User user : userList) {
            if (user.getUserName().equals(userName) && user.getPassword().equals(password)) {
                return user;
            }
        }
        return null;
    }

    public static void save() {
        FileOutputStream fos = null;
        try {
            File file = new File("Users.dat");
            fos = new FileOutputStream(file,true);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(userList);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
        } catch (IOException ex) {
        } finally {
            try {
                if (fos != null) {
                    fos.close();
                }
            } catch (IOException ex) {
            }
        }
    }


    public static void load() {
        FileInputStream fis = null;
        try {
            File file = new File("Users.dat");
            fis = new FileInputStream(file);
            ObjectInputStream ois = new ObjectInputStream(fis);
            ArrayList<User> temp = (ArrayList<User>) ois.readObject();
            UserService.addUser(temp);
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
        } catch (IOException ex) {
        } catch (ClassNotFoundException ex) {
        } finally {
            try {
                if (fis != null) {
                    fis.close();
                }
            } catch (IOException ex) {

            }
        }
    }

    private static boolean adminCheck() {
        boolean check = false;
        for (User user : userList) {
            if (user.getUserName().equals("admin")) {
                check = true;
                return check;
            }
        }
        return check;
    }

    public static boolean addUser(ArrayList<User> user) {
        userList.addAll(user);
        return true;
    }
}
